FROM openjdk:11.0.4-jre-slim-buster
COPY build/libs/demo-0.0.1-SNAPSHOT.jar app.jar
COPY conf/application.yaml application.yaml
ENTRYPOINT ["java","-Dspring.profiles.active=prd","-jar","/app.jar"]
EXPOSE 8081