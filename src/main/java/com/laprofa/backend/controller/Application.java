package com.laprofa.backend.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
Extends abstract application for METHODS
 */

@RestController
@RequestMapping("api")
public class Application {

}
