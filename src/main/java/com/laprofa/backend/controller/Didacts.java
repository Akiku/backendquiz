package com.laprofa.backend.controller;


import com.laprofa.backend.model.Didact;
import com.laprofa.backend.repository.DidactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
Class Didact refering as Teacher
 */

@RestController
public class Didacts extends Application {

    @Autowired
    DidactRepository didactRepository;

    @GetMapping("/teachers")
    public List<Didact> index() {
        return didactRepository.findAll();
    }

    @GetMapping("/teachers/{professorId}")
    public Didact getProfessorById(@PathVariable Long professorId) {
        return didactRepository.findById(professorId)
                .orElseThrow(() -> new RuntimeException("Invalid professor id: " + professorId));
    }

    @PostMapping("/teacher")
    public Didact createProfessor(@RequestBody Didact didact) {
        return didactRepository.save(didact);
    }

    @PostMapping("/teachers")
    public ResponseEntity<?> createProfessors(@RequestBody List<Didact> professori) {
        for (Didact didact : professori) {
            didactRepository.save(didact);
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping("/teacher/{professorId}")
    public Didact updateProfessor(@PathVariable Long professorId, @RequestBody Didact updateDidact) {
        return didactRepository.findById(professorId).map(didactMap -> {
            didactMap.setName(updateDidact.getName());
            didactMap.setSurname(updateDidact.getSurname());
            didactMap.setEmail(updateDidact.getEmail());
            didactMap.setAge(updateDidact.getAge());
            didactMap.setActive(updateDidact.getActive());
            return didactRepository.save(didactMap);
        }).orElseThrow(() -> new ResourceNotFoundException("Professor id "+professorId+" not found"));
    }

    @PutMapping("/teacher/status/{professorId}")
    public Didact changeStatusProfessor(@PathVariable Long professorId, @RequestBody Didact studentUpdate) {
        return didactRepository.findById(professorId).map(didactMap -> {
            didactMap.setActive(studentUpdate.getActive());
            return didactRepository.save(didactMap);
        }).orElseThrow(() -> new ResourceNotFoundException("Professor id "+professorId+" not found"));
    }

    @DeleteMapping("/teacher/{professorId}")
    public ResponseEntity<?> deleteProfessor(@PathVariable Long professorId) {
        didactRepository.deleteById(professorId);
        return ResponseEntity.ok().build();
    }


}
