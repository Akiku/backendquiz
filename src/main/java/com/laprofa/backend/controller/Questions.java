package com.laprofa.backend.controller;

import com.laprofa.backend.model.Question;
import com.laprofa.backend.repository.AnswerRepository;
import com.laprofa.backend.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/*
Rest for Questions list
 */

@RestController
public class Questions extends Application {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerRepository answerRepository;

    @GetMapping("/questions")
    private List<Question> index() {
        return questionRepository.findAll();
    }


}
