package com.laprofa.backend.controller;


import com.laprofa.backend.model.Student;
import com.laprofa.backend.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Students  extends Application{

    @Autowired
    StudentRepository studentRepository;

    @GetMapping("/students")
    public List<Student> index() {
        return studentRepository.findAll();
    }

    @GetMapping("/student/{studentId}")
    public Student getStudentById(@PathVariable Long studentId) {
        return studentRepository.findById(studentId)
                .orElseThrow(() -> new RuntimeException("Invalid student id: " + studentId));
    }

    @PostMapping("/student")
    public Student createStudent(@RequestBody Student student) {
        return studentRepository.save(student);
    }

    @PostMapping("/students")
    public ResponseEntity<?> createStudents(@RequestBody List<Student> students) {
        for (Student student : students) {
            studentRepository.save(student);
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping("/student/{studentId}")
    public Student updateStudent(@PathVariable Long studentId, @RequestBody Student studentUpdate) {
        return studentRepository.findById(studentId).map(studentMap -> {
           studentMap.setName(studentUpdate.getName());
           studentMap.setSurname(studentUpdate.getSurname());
           studentMap.setEmail(studentUpdate.getEmail());
           studentMap.setAge(studentUpdate.getAge());
           studentMap.setGrade(studentUpdate.getGrade());
           studentMap.setActive(studentUpdate.getActive());
           return studentRepository.save(studentMap);
        }).orElseThrow(() -> new ResourceNotFoundException("Student id "+studentId+" not found"));
    }

    @PutMapping("/student/status/{studentId}")
    public Student changeStatusStudent(@PathVariable Long studentId, @RequestBody Student studentUpdate) {
        return studentRepository.findById(studentId).map(studentMap -> {
            studentMap.setActive(studentUpdate.getActive());
            return studentRepository.save(studentMap);
        }).orElseThrow(() -> new ResourceNotFoundException("Student id "+studentId+" not found"));
    }

    @DeleteMapping("/student/{studentId}")
    public ResponseEntity<?> deleteStudent(@PathVariable Long studentId) {
        studentRepository.deleteById(studentId);
        return ResponseEntity.ok().build();
    }


}
