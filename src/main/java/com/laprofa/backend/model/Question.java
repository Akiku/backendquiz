package com.laprofa.backend.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String question;

    @OneToMany(mappedBy = "question")
    private List<Answer> answerList;

    private Long correctAnswer;
    private Boolean isAnswered;

}
