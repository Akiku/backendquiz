package com.laprofa.backend.repository;

import com.laprofa.backend.model.Didact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DidactRepository extends JpaRepository<Didact, Long> {

}
